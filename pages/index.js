import CreateForm from "./components/create-form";
import styles from "../styles/Home.module.css";

export default function Home() {
  return (
    <div className={styles.container}>
      <h1>Funcionários</h1>
      <CreateForm></CreateForm>
      <div className={styles.tableContainer}>
        <p>Table</p>
      </div>
    </div>
  );
}
